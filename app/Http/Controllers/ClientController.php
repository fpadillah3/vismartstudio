<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.client.index', [
            'Clients' => Client::all()
        ]);
    }

    public function data()
    {
        $client = Client::orderBy('id', 'desc')->get();

        return datatables()
            ->of($client)
            ->addIndexColumn()
            ->addColumn('aksi', function ($client) {
                return '
                    <button data-client="'.$client->nama.'" data-route="' . route('client.update', $client->id) . '" class="edit btn btn-xs btn-success btn-flat"><i class="bi bi-pencil-square"> Edit</i></button>
                    <button onclick="deleteData(`'. route('client.destroy', $client->id) .'`)" class="btn btn-xs btn-danger btn-flat delete"><i class="bi bi-trash"> Hapus</i></button>
                ';
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.client.index', [
            'Clients' => Client::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'logo' => 'required|image|file|max:5120',
            'nama' => 'required|max:225'
        ]);

        if($request->file('logo')) {
            $validatedData['logo'] = $request->file('logo')->store('client-logo');
        }

        Client::create($validatedData);

        return redirect('/dashboard/client')->with('success', 'New Client has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $rules = [
            'logo' => 'required|image|file|max:5120',
            'nama' => 'required|max:225'
        ];
        

        $validatedData = $request->validate($rules);

        if($request->file('logo')) {
            if($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $validatedData['logo'] = $request->file('logo')->store('client-logo');
        }

        Client::where('id', $client->id)
            ->update($validatedData);

        return redirect('/dashboard/client')->with('success', 'Client has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        if($client->logo) {
            Storage::delete($client->logo);
        }

        Client::destroy($client->id);

        return redirect('/dashboard/client')->with('success', 'Post has been deleted!');
    }
}
