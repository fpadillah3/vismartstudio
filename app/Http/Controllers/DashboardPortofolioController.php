<?php

namespace App\Http\Controllers;

use App\Models\Portofolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DashboardPortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.portofolios.index', [
            'portofolio' => Portofolio::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.portofolios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate ([
            'service_id' => 'required|unique:portofolios|max:255',
            'title' => 'required|max:255',
            'image' => 'image|file|max:10240'
        ]);

        if ($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('portofolio-images');
        }

        Portofolio::create($validatedData);
        return redirect('/dashboard/portofolios')->with('success', 'New portofolio has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Portofolio  $portofolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portofolio $portofolio)
    {
        return view('dashboard.portofolios.show', [
            'portofolio' => $portofolio
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Portofolio  $portofolio
     * @return \Illuminate\Http\Response
     */
    public function edit(Portofolio $portofolio)
    {
        return view('dashboard.portofolios.edit', [
            'portofolio' => $portofolio
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Portofolio  $portofolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Portofolio $portofolio)
    {
        $rules = [
            'title' => 'required|max:255',
            'image' => 'image|file|max:10240'
        ];

        if ($request->services_id != $portofolio->services_id) {
            $rules['services_id'] = 'required|unique:portofolios|max:255';
        }
        
        $validatedData = $request->validate($rules);

        if ($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('portofolio-images');
        }

        if ($request->file('image')) {
            if($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $validatedData['image'] = $request->file('image')->store('portofolio-images');
        }

        Portofolio::where('id', $portofolio->id)->update($validatedData);

        return redirect('/dashboard/portofolios')->with('success', 'Portofolio has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Portofolio  $portofolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portofolio $portofolio)
    {
        if($portofolio->image) {
            Storage::delete($portofolio->image);
        }

        Portofolio::destroy($portofolio->id);
        return redirect('/dashboard/portofolios')->with('success', 'Portofolio has been deleted!');
    }
}
