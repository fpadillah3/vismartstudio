<?php

use App\Http\Controllers\DashboardPortofolioController;
use App\Http\Controllers\DashboardCompanyController;
use App\Http\Controllers\DashboardClientController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;

use App\Http\Middleware\IsAdmin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        'title' => 'Home'
    ]);
});
Route::get('/marketingcommunications', function () {
    return view('marketing_communications', [
        'title' => 'Marketing Communications'
    ]);
});
Route::get('/designfeed', function () {
    return view('design_feed_instagram', [
        'title' => 'Design Feed Instagram'
    ]);
});
Route::get('/digitalmarketing', function () {
    return view('digital_marketing', [
        'title' => 'Digital Marketing'
    ]);
});
Route::get('/smm', function () {
    return view('smm', [
        'title' => 'Social Media Management'
    ]);
});
Route::get('/logobranding', function () {
    return view('logo_branding', [
        'title' => 'Logo dan Branding'
    ]);
});
Route::get('/posts', function () {
    return view('posts', [
        'title' => 'Posts'
    ]);
});
Route::get('/post', function () {
    return view('post', [
        'title' => 'Detail'
    ]);
});
Route::get('/category', function () {return view('category', ['title' => 'Category']);});

Route::get('/template', function () {
    return view('template', [
        'title' => 'Template'
    ]);
});
Route::get('/portofolio', function () {
    return view('portofolio', [
        'title' => 'Portofolio'
    ]);
});
Route::get('/subscribe', function () {
    return view('subscribe', [
        'title' => 'Subscribe'
    ]);
});
Route::get('/pricing', function () {
    return view('pricing', [
        'title' => 'Pricing'
    ]);
});

Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/login', [LoginController::class, 'index'])->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);

Route::post('/logout', [LoginController::class, 'logout']);

// Route::get('/dashboard', DashboardController::class)->middleware('admin');

Route::get('/dashboard', function() {
    return view('dashboard.index');
})->middleware('admin');

Route::resource('/dashboard/portofolios', DashboardPortofolioController::class)->middleware('auth');
Route::resource('/dashboard/clients', DashboardClientController::class)->middleware('auth');
Route::resource('/dashboard/companies', DashboardCompanyController::class)->middleware('auth');
