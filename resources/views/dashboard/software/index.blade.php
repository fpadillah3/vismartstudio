@extends('dashboard.layouts.main')

@section('container')

  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Templates</h1>
  </div>

  @if(session()->has('success'))
    <div class="alert alert-success col-lg-10" role="alert">
      {{ session('success') }}
    </div>
  @endif

  <div class="table-responsive col-lg-12">
    <a href="/dashboard/templates/create" class="btn btn-primary mb-3">Create new template</a>
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Logo</th>
          <th scope="col">Software</th>
          <th scope="col">Aksi</th>
        </tr>
      </thead>
      <tbody>
          @foreach ($Software as $software)   
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $software->gambar }}</td>
            <td>{{ $software->nama }}</td>
            <td>
                <a href="/dashboard/software/{{ $software->id }}" class="badge bg-info"><span data-feather="eye"></span></a>
                <a href="/dashboard/software/{{ $software->id }}/edit" class="badge bg-warning"><span data-feather="edit"></span></a>
                <form action="/dashboard/software/{{ $software->id }}" method="post" class="d-inline">
                  @csrf
                  @method('delete')
                  <button class="badge bg-danger border-0" onclick="return confirm('Are you sure?')">
                    <span data-feather="trash-2"></span>
                  </button>
                </form>
            </td>
          </tr>
          @endforeach
      </tbody>
    </table>
  </div>
    
@endsection
