@extends('dashboard.layouts.main')

@section('container')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Client</h1>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success col-lg-5" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="table-responsive col-lg-5">
        <a href="/dashboard/clients/create" class="btn btn-outline-warning mb-3">Create New Client</a>
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Company Name</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($client as $cln)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $cln->name }}</td>
                <td>{{ $cln->company_name }}</td>
                <td>
                    <a href="/dashboard/clients/{{ $cln->id }}" class="badge bg-info"><span data-feather="eye"></span></a>
                    <a href="/dashboard/clients/{{ $cln->id }}/edit" class="badge bg-warning"><span data-feather="edit"></span></a>
                    <form action="/dashboard/clients/{{ $cln->id }}" method="POST" class="d-inline">
                    @method('delete')
                    @csrf 
                    <button class="badge bg-danger border-0" onclick="return confirm('Are you sure?')">
                        <span data-feather="trash-2"></span>
                    </button>
                    </form>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection