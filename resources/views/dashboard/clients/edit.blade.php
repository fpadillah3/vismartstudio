@extends('dashboard.layouts.main')

@section('container')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Client</h1>
    </div>

    <div class="col-lg-5">
        <form method="POST" action="/dashboard/clients/{{ $client->id }}" class="mb-5" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required autofocus value="{{ old('name', $client->name) }}">
                @error('name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="company_name" class="form-label">Company Name</label>
                <input type="text" class="form-control @error('company_name') is-invalid @enderror" id="company_name" name="company_name" required autofocus value="{{ old('company_name', $client->company_name) }}">
                @error('company_name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="mb-2">
                <label for="image" class="form-label">Client Image</label>
                <input type="hidden" name="oldImage" value="{{ $client->image }}">
                @if ($client->image)
                    <img src="{{ asset('storage/' . $client->image) }}" class="img-preview img-fluid mb-2 col-sm-5 d-block">
                @else
                    <img class="img-preview img-fluid mb-2 col-sm-5">
                @endif
                
                <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image" onchange="previewImage()">
                @error('image')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-outline-warning">Update Client</button>
        </form>
    </div>
@endsection