@extends('dashboard.layouts.main')

@section('container')
<div class="container">
    <div class="row my-3">
        <div class="col-lg-5">
            <h1 class="mb-3">Client</h1>

            <table class="table table-striped table-sm mb-3">
                <thead>
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Company Name</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $client->id }}</td>
                    <td>{{ $client->name }}</td>
                    <td>{{ $client->company_name }}</td>
                </tr>
                </tbody>
            </table>

            <img src="{{ asset('storage/' . $client->image) }}" alt="{{ $client->id }}" class="img-fluid d-block mb-2">

            <a href="/dashboard/clients" class="btn btn-outline-info"><span data-feather="arrow-left"></span> Back to clients</a>
            <a href="/dashboard/clients/{{ $client->id }}/edit" class="btn btn-outline-warning"><span data-feather="edit"></span> Edit</a>
            <form action="/dashboard/clients/{{ $client->id }}" method="POST" class="d-inline">
            @method('delete')
            @csrf 
                <button class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                <span data-feather="trash-2"></span> Delete</button>
            </form>
        </div>
    </div>
</div> 
@endsection