@extends('dashboard.layouts.main')

@section('container')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Portofolio</h1>
    </div>

    <div class="col-lg-5">
        <form method="POST" action="/dashboard/portofolios/{{ $portofolio->id }}" class="mb-5" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="mb-3">
                <label for="service_id" class="form-label">Service ID</label>
                <input type="text" class="form-control @error('service_id') is-invalid @enderror" id="service_id" name="service_id" required autofocus value="{{ old('service_id', $portofolio->service_id) }}">
                @error('service_id')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="title" class="form-label">Title</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" required autofocus value="{{ old('title', $portofolio->title) }}">
                @error('title')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
    
            <div class="mb-2">
            <label for="image" class="form-label">Portofolio Image</label>
            <input type="hidden" name="oldImage" value="{{ $portofolio->image }}">
            @if ($portofolio->image)
                <img src="{{ asset('storage/' . $portofolio->image) }}" class="img-preview img-fluid mb-2 col-sm-5 d-block">
            @else
                <img class="img-preview img-fluid mb-2 col-sm-5">
            @endif
            
            <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image" onchange="previewImage()">
                @error('image')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
    
            <button type="submit" class="btn btn-outline-warning">Update Portofolio</button>
        </form>
    </div>

    <script>
        function previewImage() {
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');
    
            imgPreview.style.display = 'block';
    
            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);
    
            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endsection