@extends('dashboard.layouts.main')

@section('container')
<div class="container">
    <div class="row my-3">
        <div class="col-lg-8">
            <h1 class="mb-3">{{ $portofolio->title }}</h1>
            <img src="{{ asset('storage/' . $portofolio->image) }}" alt="{{ $portofolio->id }}" class="img-fluid d-block mb-2">

            <a href="/dashboard/portofolios" class="btn btn-outline-info"><span data-feather="arrow-left"></span> Back to portofolios</a>
            <a href="/dashboard/portofolios/{{ $portofolio->id }}/edit" class="btn btn-outline-warning"><span data-feather="edit"></span> Edit</a>
            <form action="/dashboard/portofolios/{{ $portofolio->id }}" method="POST" class="d-inline">
                @method('delete')
                @csrf 
                <button class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                <span data-feather="trash-2"></span> Delete</button>
            </form>
        </div>
    </div>
</div> 
@endsection