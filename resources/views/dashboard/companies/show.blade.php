@extends('dashboard.layouts.main')

@section('container')
<div class="container">
    <div class="row my-3">
        <div class="col-lg-7">
            <h1 class="mb-3">Company</h1>

            <h3>Name : {{ $company->name }}</h3>
            <h3>Email : {{ $company->email }}</h3>
            <h3>Whatsapp : {{ $company->whatsapp }}</h3>
            <h3>Description : {{ $company->description }}</h3>

            <a href="/dashboard/companies" class="btn btn-outline-info"><span data-feather="arrow-left"></span> Back to companies</a>
            <a href="/dashboard/companies/{{ $company->id }}/edit" class="btn btn-outline-warning"><span data-feather="edit"></span> Edit</a>
            <form action="/dashboard/companies/{{ $company->id }}" method="POST" class="d-inline">
            @method('delete')
            @csrf 
                <button class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                <span data-feather="trash-2"></span> Delete</button>
            </form>
        </div>
    </div>
</div> 
@endsection