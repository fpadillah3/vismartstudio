@can('admin')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">        
      {{-- <h6 class="sidebar-heading ">
        <span data-feather="plus-circle"><a class="nav-link" href="#"></a></span>
      </h6> --}}
          <ul class="nav flex-column">          
            <li class="nav-item">
                <a class="nav-link {{ Request::is('dashboard') ? 'active' : ''}}" aria-current="page" href="/dashboard">
                  <span data-feather="home"></span>
                Dashboard
              </a>
            </li>
            <li><span class="d-flex px-3 py-2 text-muted">Administrator</span></li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('dashboard/portofolios*') ? 'active' : ''}}" href="/dashboard/portofolios">
                <span data-feather="file-text"></span>
                Portofolio
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('dashboard/companies*') ? 'active' : ''}}" href="/dashboard/companies">
                <span data-feather="globe"></span>
                Company
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('dashboard/clients*') ? 'active' : '' }}" href="/dashboard/clients">
                <span data-feather="users"></span>
                Clients
              </a>
            </li>
          </ul>
          
          <div class="col-sm-8">
            <a class="nav-link {{ Request::is('dashboard/service*') ? 'active' : '' }}" href="/dashboard/services">
            <span data-feather="grid"></span>Services
            </a>
              
          </div>
    </div>
</nav>
@endcan