{{-- 
    <footer class="text-center text-lg-start text-white">
        <section class="p-2">
            <div class="container text-center text-md-start mt-5">
                <div class="row">
                    <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                        <p class="fs-4 m-0">2022 Madtive Studio</p>
                    </div>
                    <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

                    </div>
                    <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

                        <img src="/img/logo vismart studio white.png" class="card-img-top" alt="..." style="width: 200px">
                    </div>
                    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                    </div>
                </div>
            </div>
        </section>
    </footer> --}}

<section class="footer">
    <div class="container">
        <div class="row justify-content-center text-center align-items-center py-5">
            <div class="col-lg-6 p-3 p-0">
                <p class="fs-4 m-0">2022 Madtive Studio</p>
            </div>
            <div class="col-lg-6 p-3">
                <img src="/img/logo vismart studio white.png" class="card-img-top" alt="..." style="width: 200px">
            </div>
        </div>
    </div>
</section>
