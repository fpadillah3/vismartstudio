@extends('layouts.main')

@section('content')

<section class="header">
    <div class="container">
        <div class="row text-center align-items-center justify-content-center" style="min-height: 100vh">
            <div class="col-12">
                <img src="/img/logo vismart studio.png" class="card-img-top" alt="..." style="width: 200px">
                <h2 class="mt-5 mb-3">SERAHKAN KEPADA AHLINYA. <br>ANDA 100% FOKUS MENGURUS BISNIS ANDA.</h2>
                <p class="fs-4 mb-5">Biarkan kami yang mengurus design sosial media kamu, <br>dikerjakan oleh ahlinya tidak menggunakan Template.</p>
                <a href="#page-1"><button type="button" class="btn-border-primary btn rounded-pill border-3 p-3 px-5">REACH US!</button></a>
            </div>
        </div>
    </div>
</section>

<section class="page-1" id="page-1">
    <div class="container">
        <div class="row align-items-center justify-content-center py-5" style="min-height: 100vh; margin-bottom: 10em;">
            <div class="col-lg-6">
                <h2 class="fw-bold">Apa itu Desain Feed Instagram?</h2><br>
                <p class="fs-4">Kami adalah tim kreatif dari PT Madtive Studio Indonesia yang didirikan sejak tahun 2015 yang sudah bekerjasama dengan banyak client mulai dari UMKM, Perusahaan menengah keatas hingga pemerintahan.</p>
                <p class="fs-4">Sehingga kami sudah sangat siap untuk membantu apapun jenis bisnis kamu untuk tumbuh dan berkembang dengan konten yang terbaik dan profesional untuk target market kamu.</p><br>
                <a href="#page-2"><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
            </div>
            <div class="col-lg-6">
                <img src="/img/Design Feed Instagram.png" class="img-fluid" alt="..." style="width: 40em">
            </div>
        </div>
    </div>
</section>

<section class="page-2" id="page-2">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh; margin-bottom: 10em;">
            <h2 class="fw-bold my-5">Apa yang bisa Vismart Studio lakukan untuk Bisnismu</h2>
            <div class="col-12">
            
            <div class="row my-5 py-5" id="membuat_design_yang_profesional">
                <div class="col-lg-12">
                    <div class="row align-items-center justify-content-start">
                        <div class="col-lg-1">
                            <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 5em">
                        </div>
                        <div class="col-lg-5 text-center text-lg-start">
                            <h3 class="fw-bold">Membuat Desain yang Profesional</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-start">
                    <h2 class="fw-bold">Dengan konten-konten yang Profesional, Bisnismu bisa bertumbuh secara Organik</h2><br>
                    <ul>
                        <li class="fs-4">Design dibuat Manual, No Template dengan menggunakan Software Design Profesional (Photoshop, Ilustrator, Corel, dan Figma).</li>
                        <li class="fs-4">Dikerjakan oleh Tim Profesional.</li>
                        <li class="fs-4">100% Konsep bisa dibantu oleh Tim Visualabs.</li>
                    </ul><br>
                    <a href="#content_dibuat_dengan_profesional"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
                </div>
                <div class="col-lg-6">
                    <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 25em">
                </div>
            </div>

            <div class="row my-5 py-5" id="content_dibuat_dengan_profesional">
                <div class="col-lg-12">
                    <div class="row align-items-center justify-content-end">
                        <div class="col-lg-1">
                            <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 5em">
                        </div>
                        <div class="col-lg-5 text-center text-lg-start">
                            <h3 class="fw-bold">Content dibuat dengan Profesional</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 25em">
                </div>
                <div class="col-lg-6 text-start">
                    <h2 class="fw-bold">Semua konten untuk Design Anda dibuat berdasarkan Pengalaman & Riset yang Mendalam</h2><br>
                    <ul>
                        <li class="fs-4">Team Content Planner Visualabs sudah dilatih dalam membuat konten yang profesional untuk semua client.</li>
                        <li class="fs-4">Konten dibuat dengan tujuan agar bisa mendatangkan sales & meningkatkan interaksi.</li>
                        <li class="fs-4">Team Content Planner akan memberikan Preview Content kepada Client setelah full membuat Plannernya.</li>
                    </ul><br>
                    <a href="#client_bebas_request_apapun"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
                </div>
            </div>

            <div class="row my-5 py-5" id="client_bebas_request_apapun">
                <div class="col-lg-12">
                    <div class="row align-items-center justify-content-start">
                        <div class="col-lg-1">
                            <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 5em">
                        </div>
                        <div class="col-lg-5 text-center text-lg-start">
                            <h3 class="fw-bold">Client Bebas Request Apapun</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-start">
                    <h2 class="fw-bold">Sebelum Planner & Design dikerjakan, Client bebas Request hal apapun seputar Projectnya</h2><br>
                    <ul>
                        <li class="fs-4">Client boleh request agar di semua konten ada Logo Brand Client.</li>
                        <li class="fs-4">Client boleh request Visual & Layoutnya sesuai dengan Referensi yang Client inginkan.</li>
                        <li class="fs-4">Client boleh request jika ada goal-goal khusus untuk kontennya.</li>
                    </ul><br>
                    <a href="#desain_bisa_digunakan_ke_platform_lain"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
                </div>
                <div class="col-lg-6">
                    <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 25em">
                </div>
            </div>

            <div class="row my-5 py-5" id="desain_bisa_digunakan_ke_platform_lain">
                <div class="col-lg-12">
                    <div class="row align-items-center justify-content-end">
                        <div class="col-lg-1">
                            <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 5em">
                        </div>
                        <div class="col-lg-5 text-center text-lg-start">
                            <h3 class="fw-bold">Desain bisa digunakan ke Platform lain</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 25em">
                </div>
                <div class="col-lg-6 text-start">
                    <h2 class="fw-bold">Project Design Anda tidak hanya bisa digunakan untuk Instagram saja, tapi boleh digunakan untuk...</h2><br>
                    <ul>
                        <li class="fs-4">Iklan IG Ads & FB Ads.</li>
                        <li class="fs-4">Untuk diberikan ke Agen, Distributor, Reseller Anda.</li>
                        <li class="fs-4">Untuk Anda posting ke WA Story, FB personal, dll.</li>
                        <li class="fs-4">Untuk Anda jadikan konten di Marketplace.</li>
                    </ul><br>
                    <a href="#keuntungan_lain_yang_client_dapatkan"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
                </div>
            </div>

            <div class="row my-5 py-5" id="keuntungan_lain_yang_client_dapatkan">
                <div class="col-lg-12">
                    <div class="row align-items-center justify-content-start">
                        <div class="col-lg-1">
                            <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 5em">
                        </div>
                        <div class="col-lg-5 text-center text-lg-start">
                            <h3 class="fw-bold">Keuntungan lain yang Client dapatkan</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-start">
                    <h2 class="fw-bold">Tidak hanya value-value diatas saja, dibawah ini ada beberapa Keuntungan lain yang bisa Anda dapatkan dari Visualabs</h2><br>
                    <ul>
                        <li class="fs-4">Harga yang Super Terjangkau. Seluruh Paket Visualabs dibuat KHUSUS untuk UMKM di Indonesia, sehingga Harga yang Visualabs hadirkan juga sangat terjangkau di Kantong para UMKM yang ada.</li>
                        <li class="fs-4">FREE Revisi Desain. Jika ada kesalahan / ada yang tidak sesuai dengan brief yang sudah Anda berikan, Anda bisa mengajukan revisi untuk Project Anda.</li>
                    </ul><br>
                    <a href="#content_dibuat_dengan_profesional"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
                </div>
                <div class="col-lg-6">
                    <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 25em">
                </div>
            </div>
        
        </div>

        </div>
    </div>
</section>

<section class="column-3" id="column-3">
    <div class="container">
        <div class="row justify-content-center text-center" style="min-height: 100vh">
            <h2 class="fw-bold my-5">Apa saja konten yang akan Anda dapatkan</h2>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Content Idea.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Konten Desain Product</h4>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Content Design.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Konten Promo Product</h4>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Copywriting.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Testimoni Customer</h4>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Hashtag Research.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Konten Edukasi</h4>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Schedule Post.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Instagram Stories</h4>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Private Zoom Video.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Request Sesuka Hati</h4>
            </div>
        </div>
    </div>
</section>

<section class="pricing">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh">
            <h2 class="fw-bold my-5">Pilih Paket</h2>
            <div class="col-lg-4">
                <div class="card mb-4 py-3">
                    <h4 class="fw-bold mt-3">Keren</h4>
                    <div class="card-body">
                        <span class="h2 fw-bold">IDR 567k </span>/ Month
                            <ul class="list-group list-group-flush fa-ul text-start p-3 mb-3">
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Kamu Tinggal Terima Beres Aja!</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Posting <b>SETIAP HARI</b> Kec. Hari Libur</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Ide Konten & Materi Konten</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Copywriting konten & Caption</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Hashtag Setiap konten</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Bisa kasi konsep / Referensi design</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Admin Posting Di IG & Fb</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Revisi ( Minor ) 2x / Desain</li>
                            </ul>
                        <a href="#"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">Beli Sekarang!</button></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card mb-4 py-5"  style="color: #fff; background-color: var(--primary-color);">
                    <h4 class="fw-bold mt-3">Ajib</h4>
                    <div class="card-body">
                        <span class="h2 fw-bold">IDR 1489k </span>/ Month
                            <ul class="list-group list-group-flush fa-ul text-start p-3 mb-3">
                                <li class="list-group-item list-primary"><i class="fa-solid fa-check fa-li"></i>Kamu Tinggal Terima Beres Aja!</li>
                                <li class="list-group-item list-primary"><i class="fa-solid fa-check fa-li"></i>Posting <b>SETIAP HARI</b> Kec. Hari Libur</li>
                                <li class="list-group-item list-primary"><i class="fa-solid fa-check fa-li"></i>Ide Konten & Materi Konten</li>
                                <li class="list-group-item list-primary"><i class="fa-solid fa-check fa-li"></i>Copywriting konten & Caption</li>
                                <li class="list-group-item list-primary"><i class="fa-solid fa-check fa-li"></i>Hashtag Setiap konten</li>
                                <li class="list-group-item list-primary"><i class="fa-solid fa-check fa-li"></i>Bisa kasi konsep / Referensi design</li>
                                <li class="list-group-item list-primary"><i class="fa-solid fa-check fa-li"></i>Admin Posting Di IG & Fb</li>
                                <li class="list-group-item list-primary"><i class="fa-solid fa-check fa-li"></i>Revisi ( Minor ) 2x / Desain</li>
                            </ul>
                        <a href="#"><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5">Beli Sekarang!</button></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card mb-4 py-3">
                    <h4 class="fw-bold mt-3">Mantap</h4>
                    <div class="card-body">
                        <span class="h2 fw-bold">IDR 934k </span>/ Month
                            <ul class="list-group list-group-flush fa-ul text-start p-3 mb-3">
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Kamu Tinggal Terima Beres Aja!</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Posting <b>SETIAP HARI</b> Kec. Hari Libur</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Ide Konten & Materi Konten</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Copywriting konten & Caption</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Hashtag Setiap konten</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Bisa kasi konsep / Referensi design</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Admin Posting Di IG & Fb</li>
                                <li class="list-group-item"><i class="fa-solid fa-check fa-li"></i>Revisi ( Minor ) 2x / Desain</li>
                            </ul>
                        <a href="#"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">Beli Sekarang!</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-4" id="page-4">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh">
            <div class="col-lg-10">
                <h1 class="fw-bold my-5">Saatnya menumbuhkan Bisnismu dengan Konten-konten yang lebih Menjual & Profesional</h1>
                <p class="fs-4">Manto Mukhli Fardi</p>
                <p class="fs-4">0812 3456 7890</p>
                <a href=""><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">Chat Sekarang!</button></a>
            </div>
        </div>
    </div>
</section>

@endsection