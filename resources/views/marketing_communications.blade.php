@extends('layouts.main')

@section('content')

<section class="header">
    <div class="container">
        <div class="row text-center align-items-center justify-content-center" style="min-height: 100vh">
            <div class="col-12">
                <img src="/img/logo vismart studio.png" class="card-img-top" alt="..." style="width: 200px">
                <h2 class="mt-5 mb-3">SERAHKAN KEPADA AHLINYA. <br>ANDA 100% FOKUS MENGURUS BISNIS ANDA.</h2>
                <p class="fs-4 mb-5">Biarkan kami yang mengurus design sosial media kamu, <br>dikerjakan oleh ahlinya tidak menggunakan Template.</p>
                <a href="#page-1"><button type="button" class="btn-border-primary btn rounded-pill border-3 p-3 px-5">REACH US!</button></a>
            </div>
        </div>
    </div>
</section>

<section class="page-1" id="page-1">
    <div class="container">
        <div class="row align-items-center justify-content-center py-5" style="min-height: 100vh; margin-bottom: 10em;">
            <div class="col-lg-6 p-5 order-md-last">
                <img src="/img/Marketing Communications.png" class="img-fluid" alt="..." style="width: 40em">
            </div>
            <div class="col-lg-6 p-5">
                <h2 class="fw-bold">Apa itu Marketing Communications?</h2><br>
                <p class="fs-4">Pada dasarnya, komunikasi adalah proses mengirim pesan dan informasi antara pengirim dan penerimanya. Komunikasi dapat dikatakan sukses jika kedua pihak tersebut bisa mengerti dan mengolah pesan yang disampaikan.</p>
                <p class="fs-4">Demikian juga dengan marketing communication. Agar calon konsumen bersedia mengeluarkan uang untuk menggunakan produk dan layanan jasa yang ditawarkan, diperlukan pengertian yang sama. Di mana produk dan layanan jasa yang bersangkutan adalah solusi dari problem mereka.</p><br>
                <a href="#page-2"><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
            </div>
        </div>
    </div>
</section>

<section class="page-2" id="page-2">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh; margin-bottom: 10em;">
            <h1 class="fw-bold my-5">Mengapa Marketing Communications Itu Penting?</h1>
            <div class="col-12">
            
            <div class="row my-5 py-5" id="membentuk_brand_awerness">
                <div class="col-lg-6 p-5 order-md-last">
                    <img src="/img/Cohort analysis-amico.png" class="img-fluid" alt="..." style="width: 25em">
                </div>
                <div class="col-lg-6 p-5 text-start">
                    <h2 class="fw-bold">Membentuk Brand Awareness</h2><br>
                    <p class="fs-4 text-black-50">Marketing communication berfungsi penting untuk membentuk brand awareness di kalangan masyarakat. Tak peduli perusahaan lama atau baru, brand awareness perlu dibangun agar suatu produk atau layanan lebih dikenal eksistensinya.</p><br>
                    <a href="#mengubah_citra_perusahaan"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
                </div>
            </div>

            <div class="row my-5 py-5" id="mengubah_citra_perusahaan">
                <div class="col-lg-6 p-5">
                    <img src="/img/Customer Survey-amico.png" class="img-fluid" alt="..." style="width: 25em">
                </div>
                <div class="col-lg-6 p-5 text-start">
                    <h2 class="fw-bold">Mengubah Citra Perusahaan</h2><br>
                    <p class="fs-4 text-black-50">Selain itu, marketing communication dapat mengubah pandangan atau persepsi publik terhadap suatu produk/ layanan. Pasalnya, beberapa kesalahpahaman berkenaan tarif, fitur, cara penggunaan dsb dari sebuah produk/ layanan kerap terjadi di kalangan masyarakat.</p><br>
                    <a href="#membujuk_konsumen"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
                </div>
            </div>

            <div class="row my-5 py-5" id="membujuk_konsumen">
                <div class="col-lg-6 p-5 order-md-last">
                    <img src="/img/Email capture-amico.png" class="img-fluid" alt="..." style="width: 25em">
                </div>
                <div class="col-lg-6 p-5 text-start">
                    <h2 class="fw-bold">Membujuk Konsumen</h2><br>
                    <p class="fs-4 text-black-50">Selanjutnya, marketing communication juga berguna untuk memotivasi konsumen agar datang lagi/ menggunakan kembali, serta melakukan pembelian berulang di kemudian hari.</p>
                    <p class="fs-4 text-black-50">Langkah ini dapat diterapkan dengan cara membuat iklan yang membujuk, dengan menggarisbawahi apa saja manfaat dan keunggulan dari penggunaan sebuah produk atau layanan jasa daripada yang ditawarkan pesaing lainnya.</p><br>
                    <a href="#page-4"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
                </div>
            </div>
        
        </div>

        </div>
    </div>
</section>

<section class="page-4" id="page-4">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh">
            <div class="col-lg-10">
                <h1 class="fw-bold my-5">Saatnya menumbuhkan Bisnismu dengan Konten-konten yang lebih Menjual & Profesional</h1>
                <p class="fs-4">Manto Mukhli Fardi</p>
                <p class="fs-4">0812 3456 7890</p>
                <a href=""><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">Chat Sekarang!</button></a>
            </div>
        </div>
    </div>
</section>

@endsection