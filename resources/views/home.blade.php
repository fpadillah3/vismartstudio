@extends('layouts.main')

@section('content')

<section class="header">
    <div class="container">
        <div class="row text-center align-items-center justify-content-center" style="min-height: 100vh">
            <div class="col-12">
                <img src="/img/logo vismart studio.png" class="card-img-top" alt="..." style="width: 300px;">
                <h2 class="my-5">One Stop Solution for Branding, Digital Marketing, <br>Social Media Management & Marketing Communication</h2>
                <a href="#page-1"><button type="button" class="btn-border-primary btn rounded-pill border-3 p-3 px-5">REACH US!</button></a>
            </div>
        </div>
    </div>
</section>

<section class="page-1" id="page-1">
    <div class="container">
        <div class="row align-items-center justify-content-center py-5" style="min-height: 100vh;">
            <div class="col-lg-6">
                <h2 class="fw-bold">Kenapa Harus Vismart Studio?</h2><br>
                <p class="fs-4">Kami adalah tim kreatif dari PT Madtive Studio Indonesia yang didirikan sejak tahun 2015 yang sudah bekerjasama dengan banyak client mulai dari UMKM, Perusahaan menengah keatas hingga pemerintahan.</p>
                <p class="fs-4">Sehingga kami sudah sangat siap untuk membantu apapun jenis bisnis kamu untuk tumbuh dan berkembang dengan konten yang terbaik dan profesional untuk target market kamu.</p><br>
                <a href="#page-2"><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
            </div>
            <div class="col-lg-6">
                <img src="/img/login.png" class="img-fluid" alt="..." style="width: 40em">
            </div>
        </div>
    </div>
</section>

<section class="page-2" id="page-2">
    <div class="container">
        <div class="row align-items-center justify-content-center" style="min-height: 100vh;">
            <div class="col-lg-6">
                <img src="/img/home.png" class="img-fluid page-2-img" alt="..." style="width: 40em">
            </div>
            <div class="col-lg-6">
                <h2 class="fw-bold my-5 px-4 text-center text-lg-start">Layanan Kami</h2>
                    <div class="service p-2 px-4 mb-4">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-lg-2 text-center">
                                <img src="/img/Logo dan Branding Icon.png" class="img-fluid" alt="..." style="width: 6em">
                            </div>
                            <div class="col-lg-10">
                                <h4 class="fw-bold">Logo dan Branding</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est architecto recusandae obcaecati dignissimos magnam minus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="service p-2 px-4 mb-4">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-lg-2 text-center">
                                <img src="/img/Desain Feed Instagram Icon.png" class="img-fluid" alt="..." style="width: 6em">
                            </div>
                            <div class="col-lg-10">
                                <h4 class="fw-bold">Desain Feed Instagram</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est architecto recusandae obcaecati dignissimos magnam minus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="service p-2 px-4 mb-4">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-lg-2 text-center">
                                <img src="/img/Digital Marketing Icon.png" class="img-fluid" alt="..." style="width: 6em">
                            </div>
                            <div class="col-lg-10">
                                <h4 class="fw-bold">Digital Marketing</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est architecto recusandae obcaecati dignissimos magnam minus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="service p-2 px-4 mb-4">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-lg-2 text-center">
                                <img src="/img/Social Media Management Icon.png" class="img-fluid" alt="..." style="width: 6em">
                            </div>
                            <div class="col-lg-10">
                                <h4 class="fw-bold">Social Media Management</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est architecto recusandae obcaecati dignissimos magnam minus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="service p-2 px-4 mb-4">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-lg-2 text-center">
                                <img src="/img/Marketing Communications Icon.png" class="img-fluid" alt="..." style="width: 6em">
                            </div>
                            <div class="col-lg-10">
                                <h4 class="fw-bold">Marketing Communications</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est architecto recusandae obcaecati dignissimos magnam minus.</p>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</section>

<section class="page-3">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh;">
            <h2 class="fw-bold my-5">Apa yang Kamu Butuhkan?</h2>
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="feature">
                    <img src="/img/Logo dan Branding Icon.png" class="img-fluid" alt="..." style="width: 15em">
                    <h1 class="fw-bold">Logo dan <br>Branding</h1>
                    <a href="logobranding"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">CLICK HERE</button></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="feature">
                    <img src="/img/Desain Feed Instagram Icon.png" class="img-fluid" alt="..." style="width: 15em">
                    <h1 class="fw-bold">Desain Feed <br>Instagram</h1>
                    <a href="designfeed"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">CLICK HERE</button></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="feature">
                    <img src="/img/Digital Marketing Icon.png" class="img-fluid" alt="..." style="width: 15em">
                    <h1 class="fw-bold">Digital <br>Marketing</h1>
                    <a href="digitalmarketing"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">CLICK HERE</button></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="feature">
                    <img src="/img/Social Media Management Icon.png" class="img-fluid" alt="..." style="width: 15em">
                    <h1 class="fw-bold">Social Media <br>Management</h1>
                    <a href="smm"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">CLICK HERE</button></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="feature">
                    <img src="/img/Marketing Communications Icon.png" class="img-fluid" alt="..." style="width: 15em">
                    <h1 class="fw-bold">Marketing <br>Communications</h1>
                    <a href="marketingcommunications"><button type="button" class="btn-primary btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">CLICK HERE</button></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-4" id="page-4">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh">
            <div class="col-lg-8">
                <h1 class="fw-bold my-5">Konsul yuk terkait bisnis dan branding dari produk kamu!</h1>
                <p class="fs-4">Manto Mukhli Fardi</p>
                <p class="fs-4">0812 3456 7890</p>
                <a href=""><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">Chat Sekarang!</button></a>
            </div>
        </div>
    </div>
</section>

@endsection