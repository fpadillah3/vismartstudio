@extends('layouts.main')

@section('content')

<section class="header">
    <div class="container">
        <div class="row text-center align-items-center justify-content-center" style="min-height: 100vh">
            <div class="col-12">
                <img src="/img/logo vismart studio.png" class="card-img-top" alt="..." style="width: 200px">
                <h2 class="mt-5 mb-3">Now Managing Instagram Business is Easier Without Having a <br>Team, Thinking About Content and Design</h2>
                <p class="fs-4 mb-5">We've Experience about Creative Content, Viral Content & Brand Hacking!</p>
                <a href="#page-1"><button type="button" class="btn-border-primary btn rounded-pill border-3 p-3 px-5">REACH US!</button></a>
            </div>
        </div>
    </div>
</section>

<section class="page-1" id="page-1">
    <div class="container">
        <div class="row align-items-center justify-content-center py-5" style="min-height: 100vh; margin-bottom: 10em;">
            <div class="col-lg-6">
                <h2 class="fw-bold">Apa itu Social Media Management?</h2><br>
                <p class="fs-4">Pada intinya, social media management adalah penggunaan berbagai tools, software maupun layanan yang mampu membantu para pebisnis dalam membagikan konten bisnisnya di media sosial.</p>
                <p class="fs-4">Beberapa hal yang bisa dikategorikan dalam kegiatan social media management adalah menjadwalkan waktu posting di media sosial, melakukan interaksi dengan target audience, sampai mengelola respon di media sosial secara cepat.</p><br>
                <a href="#page-2"><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
            </div>
            <div class="col-lg-6">
                <img src="/img/Digital Marketing.png" class="img-fluid" alt="..." style="width: 40em">
            </div>
        </div>
    </div>
</section>

<section class="page-2" id="page-2">
  <div class="container">
      <div class="row align-items-center justify-content-around text-center">
        <h2 class="fw-bold my-5">We'll Handle 100% All of About You!</h2>
          <div class="col-lg-6">
            <div class="service p-2 px-4 mb-4">
              <div class="row justify-content-center align-items-center">
                  <div class="col-lg-2 text-center">
                      <img src="/img/Logo dan Branding Icon.png" class="img-fluid" alt="..." style="width: 6em">
                  </div>
                  <div class="col-lg-10 text-start">
                    <h4 class="fw-bold">100% from Vismart Studio</h4>
                    <p>All of them, from concept, idea, property. Visualabs, will helping you.</p>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="service p-2 px-4 mb-4">
              <div class="row justify-content-center align-items-center">
                  <div class="col-lg-2 text-center">
                      <img src="/img/Logo dan Branding Icon.png" class="img-fluid" alt="..." style="width: 6em">
                  </div>
                  <div class="col-lg-10 text-start">
                    <h4 class="fw-bold">Free Design Revision</h4>
                    <p>Want to revision? No worries! Will giving you about revision like yours one time revision for each design.</p>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="service p-2 px-4 mb-4">
              <div class="row justify-content-center align-items-center">
                  <div class="col-lg-2 text-center">
                      <img src="/img/Logo dan Branding Icon.png" class="img-fluid" alt="..." style="width: 6em">
                  </div>
                  <div class="col-lg-10 text-start">
                    <h4 class="fw-bold">NO Template - From Scracth</h4>
                    <p>We can guarantee, all design will create from zero. Not using template from anywhere places.</p>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="service p-2 px-4 mb-4">
              <div class="row justify-content-center align-items-center">
                  <div class="col-lg-2 text-center">
                      <img src="/img/Logo dan Branding Icon.png" class="img-fluid" alt="..." style="width: 6em">
                  </div>
                  <div class="col-lg-10 text-start">
                    <h4 class="fw-bold">Discussions with teams Vismart Studio</h4>
                    <p>You can discuss anything about your content & Instagram with all the visualabs team.</p>
                  </div>
              </div>
            </div>
          </div>
      </div>
  </div>
</section>

<section class="column-3" id="column-3">
    <div class="container">
        <div class="row justify-content-around text-center" style="min-height: 100vh">
            <h2 class="fw-bold my-5">Why Must Social Media Management By Vismart Studio</h2>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Content Idea.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Content Idea</h4>
                <p>Various Interactive and Educational Content Ideas that can increase brand awareness</p>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Content Design.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Content Design</h4>
                <p>Content visualized in the form of creative designs by professional graphic designers</p>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Copywriting.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Copywriting</h4>
                <p>Persuasive and powerful copywriting techniques tailored to your target market</p>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Hashtag Research.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Hashtag Research</h4>
                <p>Optimizing copywriting with relevant and popular hashtags according to the business to make it easy for the audience to find</p>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Schedule Post.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Schedule Post</h4>
                <p>Schedule posts according to Instagram Business prime time consistently</p>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/Private Zoom Video.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Private  Zoom Video</h4>
                <p>In private zoom you can ask anything about marketing, branding, campaigns, etc..</p>
            </div>
        </div>
    </div>
</section>

<section class="page-4" id="page-4">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh">
            <div class="col-lg-10">
                <h1 class="fw-bold my-5">Saatnya menumbuhkan Bisnismu dengan Konten-konten yang lebih Menjual & Profesional</h1>
                <p class="fs-4">Manto Mukhli Fardi</p>
                <p class="fs-4">0812 3456 7890</p>
                <a href=""><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">Chat Sekarang!</button></a>
            </div>
        </div>
    </div>
</section>

@endsection