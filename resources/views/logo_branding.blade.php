@extends('layouts.main')

@section('content')

<section class="header">
    <div class="container">
        <div class="row text-center align-items-center justify-content-center" style="min-height: 100vh">
            <div class="col-12">
                <img src="/img/logo vismart studio.png" class="card-img-top" alt="..." style="width: 200px">
                <h2 class="mt-5 mb-3">Now Managing Instagram Business is Easier Without Having a <br>Team, Thinking
                    About Content and Design</h2>
                <p class="fs-4 mb-5">We've Experience about Creative Content, Viral Content & Brand Hacking!</p>
                <a href="#page-1"><button type="button"
                        class="btn-border-primary btn rounded-pill border-3 p-3 px-5">REACH US!</button></a>
            </div>
        </div>
    </div>
</section>

<section class="page-1" id="page-1">
    <div class="container">
        <div class="row align-items-center justify-content-center py-5" style="min-height: 100vh; margin-bottom: 10em;">
            <div class="col-lg-6">
                <h2 class="fw-bold">Mengapa Harus Memiliki Logo?</h2><br>
                <p class="fs-4">Tahukah Anda, logo merupakan unsur yang paling penting bagi sebuah perusahaan.</p>
                <p class="fs-4">Sebab, umumnya manusia itu akan cenderung lebih mudah untuk mengingat sesuatu secara
                    visual. Sehingga, desain logo yang memikat tentu akan sangat mendukung brand bisnis dalam menarik
                    minat konsumen.</p>
                <p class="fs-4">Jadi tunggu apalagi? Yuk segera pesan desain logo untuk perusahaan kamu kepada ahlinya.</p><br>
                <a href="#page-2"><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
            </div>
            <div class="col-lg-6">
                <img src="/img/logo branding.png" class="img-fluid rounded-start" alt="..." style="width: 40em">
            </div>
        </div>
    </div>
</section>

<section class="">
    <div class="container text-center py-5">
        <div class="row justify-content-center text-center">
            <h2 class="fw-bold my-5">Client Vismart Studio</h2>
            <div class="owl-carousel owl-images owl-theme">
                <div class="item"><img src="img/login.jpg" alt="Login"></div>
                <div class="item"><img src="img/register.jpg" alt="register"></div>
                <div class="item"><img src="img/login.jpg" alt="Login"></div>
                <div class="item"><img src="img/register.jpg" alt="register"></div>
                <div class="item"><img src="img/login.jpg" alt="Login"></div>
                <div class="item"><img src="img/register.jpg" alt="register"></div>
            </div>
            <h5 class="my-3">Dan masih banyak lagi</h5>
        </div>
    </div>
</section>

<section class="column-3" id="column-3">
  <div class="container">
      <div class="row justify-content-around text-center" style="min-height: 50vh">
          <h2 class="fw-bold my-5">Mengapa Anda Harus Menggunakan Jasa Logo dan Branding dari Vismart Studio</h2>
          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/proses cepat.png" class="img-fluid mb-3" alt="..." style="width: 5em">
              <h4 class="fw-bold">Proses Cepat</h4>
              <p>Dengan sistem kerja tim yang efisien dan berusaha memberi yang terbaik untuk klien kami.</p>
          </div>
          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/file master.png" class="img-fluid mb-3" alt="..." style="width: 5em">
              <h4 class="fw-bold">File Master</h4>
              <p>File master edit table diberikan untuk mempermudah klien mencetak dan memperbaiki file.</p>
          </div>
          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/konsultasi.png" class="img-fluid mb-3" alt="..." style="width: 5em">
              <h4 class="fw-bold">Konsultasi</h4>
              <p>Anda bebas konsultasi dengan <br> tim kami agar tim kami memahami kebutuhan anda.</p>
          </div>
      </div>
  </div>
</section>

<section class="column-3" id="column-3">
  <div class="container">
      <div class="row justify-content-center text-center" style="min-height: 50vh">
          <h2 class="fw-bold my-5">Layanan Jasa Desain Logo</h2>
          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/logo umkm.png" class="img-fluid rounded-start mb-3" alt="Logo UMKM" style="width: 15em">
              <h4 class="fw-bold">Logo UMKM</h4>
              <p>UMKM harus naik kelas, salah satu kuncinya adalah branding, dan logo adalah salah satunya.</p>
          </div>
          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/logo website.png" class="img-fluid rounded-start mb-3" alt="Logo Web" style="width: 15em">
              <h4 class="fw-bold">Logo Web</h4>
              <p>Logo dari web itu sendiri penting, menggambarkan produk yang anda produksi.</p>
          </div>
          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/logo online shop.png" class="img-fluid rounded-start mb-3" alt="Logo Online Shop" style="width: 15em">
              <h4 class="fw-bold">Logo Online Shop</h4>
              <p>Bisnis online shop mulai bangkit, walaupun hanya dropship sudah sepatutnya memiliki logo.</p>
          </div>

          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/logo waralaba.png" class="img-fluid rounded-start mb-3" alt="Logo Waralaba" style="width: 15em">
              <h4 class="fw-bold">Logo Waralaba</h4>
              <p>Anda pemilik waralaba, logo unik dengan warna yang tegas adalah ciri khas dari branding waralaba.</p>
          </div>
          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/logo company.png" class="img-fluid rounded-start mb-3" alt="Logo Perusahaan" style="width: 15em">
              <h4 class="fw-bold">Logo Perusahaan</h4>
              <p>Untuk pengajuan legalitas perusahaan, logo adalah salah satu syarat yang harus dimiliki.</p>
          </div>
          <div class="col-lg-4 col-md-6 p-5">
              <img src="/img/logo lembaga.png" class="img-fluid rounded-start mb-3" alt="Logo Lembaga" style="width: 15em">
              <h4 class="fw-bold">Logo Lembaga</h4>
              <p>Dalam sebuah lembaga, logo sering juga disebut sebagai lambang, Vismart Studio menerima jasa pembuatan logo lembaga.</p>
          </div>
      </div>
  </div>
</section>

<section class="page-5" id="page-5">
  <div class="container text-center">
      <div class="row justify-content-center text-center">
          <h2 class="fw-bold my-5">Portofolio Vismart Studio</h2>
          <div class="owl-carousel owl-images owl-theme">
              <div class="item"><img src="img/marhamah.png" alt="Klinik Marhamah"></div>
              <div class="item"><img src="img/keluarga.png" alt="Klinik Keluarga"></div>
              <div class="item"><img src="img/marhamah.png" alt="Klinik Marhamah"></div>
              <div class="item"><img src="img/keluarga.png" alt="Klinik Keluarga"></div>
              <div class="item"><img src="img/marhamah.png" alt="Klinik Marhamah"></div>
              <div class="item"><img src="img/keluarga.png" alt="Klinik Keluarga"></div>
          </div>
      </div>
  </div>
</section>

<section class="page-6" id="page-6">
  <div class="container text-center">
      <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh">
        <div class="col-8">
          <div class="owl-carousel owl-text owl-theme">
              <div class="item">
                <h2><b>" Keren banget Vismart Studio, hasilnya mantep berasa punya tim desain profesional sendiri "</b></h2>
                <h4><b>Fajar Padillah</b></h4>
                <h5>@fjar9</h5>
              </div>
              <div class="item">
                <h2><b>" Keren banget Vismart Studio, hasilnya mantep berasa punya tim desain profesional sendiri "</b></h2>
                <h4><b>Fajar Padillah</b></h4>
                <h5>@iqbal</h5>
              </div>
              <div class="item">
                <h2><b>" Keren banget Vismart Studio, hasilnya mantep berasa punya tim desain profesional sendiri "</b></h2>
                <h4><b>Fajar Padillah</b></h4>
                <h5>@luhung</h5>
              </div>
              <div class="item">
                <h2><b>" Keren banget Vismart Studio, hasilnya mantep berasa punya tim desain profesional sendiri "</b></h2>
                <h4><b>Fajar Padillah</b></h4>
                <h5>@mita</h5>
              </div>
              <div class="item">
                <h2><b>" Keren banget Vismart Studio, hasilnya mantep berasa punya tim desain profesional sendiri "</b></h2>
                <h4><b>Fajar Padillah</b></h4>
                <h5>@fjar9</h5>
              </div>
              <div class="item">
                <h2><b>" Keren banget Vismart Studio, hasilnya mantep berasa punya tim desain profesional sendiri "</b></h2>
                <h4><b>Fajar Padillah</b></h4>
                <h5>@fjar9</h5>
              </div>
          </div>
        </div>
      </div>
  </div>
</section>

<section class="page-4" id="page-4">
  <div class="container">
      <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh">
          <div class="col-lg-10">
              <h1 class="fw-bold my-5">Saatnya menumbuhkan Bisnismu dengan Konten-konten yang lebih Menjual & Profesional</h1>
              <p class="fs-4">Manto Mukhli Fardi</p>
              <p class="fs-4">0812 3456 7890</p>
              <a href=""><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">Chat Sekarang!</button></a>
          </div>
      </div>
  </div>
</section>


@endsection

@push('scripts')
    <script>
        $(".owl-images").owlCarousel({
            loop:true,
            margin:30,
            nav:true,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,

            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        })

        $(".owl-text").owlCarousel({
            loop:true,
            margin:30,
            nav:true,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,

            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        })

    </script>
@endpush