@extends('layouts.main')

@section('content')

<section class="header">
    <div class="container">
        <div class="row text-center align-items-center justify-content-center" style="min-height: 100vh">
            <div class="col-12">
                <img src="/img/logo vismart studio.png" class="card-img-top" alt="..." style="width: 200px">
                <h2 class="mt-5 mb-3">Now Managing Instagram Business is Easier Without Having a <br>Team, Thinking About Content and Design</h2>
                <p class="fs-4 mb-5">We've Experience about Creative Content, Viral Content & Brand Hacking!</p>
                <a href="#page-1"><button type="button" class="btn-border-primary btn rounded-pill border-3 p-3 px-5">REACH US!</button></a>
            </div>
        </div>
    </div>
</section>

<section class="page-1" id="page-1">
    <div class="container">
        <div class="row align-items-center justify-content-center py-5" style="min-height: 100vh; margin-bottom: 10em;">
            <div class="col-lg-6">
                <h2 class="fw-bold">Apa itu Digital Marketing?</h2><br>
                <p class="fs-4">Apa itu digital marketing? Pengertian digital marketing adalah suatu strategi pemasaran menggunakan media digital dan internet.</p>
                <p class="fs-4">Apakah Anda pernah melihat suatu brand atau produk perusahaan yang melakukan digital campaign atau kampanye online? Atau mungkin Anda juga pernah melihatnya melalui website dan social media suatu perusahaan?</p>
                <p class="fs-4">Seiring dengan kemajuan teknologi, tren di dunia bisnis juga semakin bervariasi. Salah satunya adalah tren digital marketing.</p><br>
                <a href="#column-3"><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5">NEXT</button></a>
            </div>
            <div class="col-lg-6">
                <img src="/img/Digital Marketing.png" class="img-fluid" alt="..." style="width: 40em">
            </div>
        </div>
    </div>
</section>

<section class="column-3" id="column-3">
    <div class="container">
        <div class="row justify-content-around text-center" style="min-height: 50vh">
            <h2 class="fw-bold my-5">3 Keunggulan Layanan Digital Marketing Vismart Studio</h2>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/berpengalaman.png" class="img-fluid mb-3" alt="..." style="width: 5em">
                <h4 class="fw-bold">Berpengalaman</h4>
                <p>Tim Digital kami menggabungkan praktik terbaik pemasaran online dengan skill teknis yang cerdas.</p>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/terbaik.png" class="img-fluid mb-3" alt="..." style="width: 5em">
                <h4 class="fw-bold">Layanan Digital Terbaik</h4>
                <p>Kami telah melalui 3 tahap penilaian yang ketat untuk memberi praktik promosi online terbaik.</p>
            </div>
            <div class="col-lg-4 col-md-6 p-5">
                <img src="/img/transparan.png" class="img-fluid mb-3" alt="..." style="width: 5em">
                <h4 class="fw-bold">Transparan</h4>
                <p>Kami transparan dan kami memisahkan biaya jasa digital marketing dengan pengeluaran promosi anda.</p>
            </div>
        </div>
    </div>
</section>

<section class="column-2" id="column-2">
    <div class="container">
        <div class="row justify-content-around text-center" style="min-height: 100vh">
            <h2 class="fw-bold my-5">Cara Kerja Kami</h2>
            <div class="col-lg-5 p-5 mx-5">
                <img src="/img/Fase Attract.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Fase Attract (Memikat)</h4>
                <p>Kami berfokus untuk anda melakukan promosi secara online pada target pasar yang tepat. Tujuan fase ini adalah pengunjung yang potensial bagi situs anda.</p>
            </div>
            <div class="col-lg-5 p-5 mx-5">
                <img src="/img/Fase Convert.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Fase Convert (Konversi)</h4>
                <p>File master editable di berikan untuk mempermudah klien mencetak dan memperbaiki file.</p>
            </div>
            <div class="col-lg-5 p-5 mx-5">
                <img src="/img/Fase Close.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Fase Close (Transaksi)</h4>
                <p>Bagi bisnis online, dalam fase ini kami akan menyusun aktivitas promo online dengan menggabungkan layanan sebelumnya dengan tujuan menaikkan penjualan.</p>
            </div>
            <div class="col-lg-5 p-5 mx-5">
                <img src="/img/Fase Delight.png" class="img-fluid mb-3" alt="..." style="width: 15em">
                <h4 class="fw-bold">Fase Delight & Viral</h4>
                <p>Kami meyakini bahwa pelanggan yang puas akan berbagi informasi dan ini merupakan metoda paling baik untuk mendapatkan lebih banyak pelanggan baru.</p>
            </div>
        </div>
    </div>
</section>



<section class="page-4" id="page-4">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center" style="min-height: 100vh">
            <div class="col-lg-10">
                <h1 class="fw-bold my-5">Saatnya menumbuhkan Bisnismu dengan Konten-konten yang lebih Menjual & Profesional</h1>
                <p class="fs-4">Manto Mukhli Fardi</p>
                <p class="fs-4">0812 3456 7890</p>
                <a href=""><button type="button" class="btn-white btn rounded-pill border-3 p-3 px-5 mt-3 mb-5">Chat Sekarang!</button></a>
            </div>
        </div>
    </div>
</section>

@endsection