<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::create([
            'name' => 'Fajar Padilah',
            'username' => 'fjzar9',
            'password' => bcrypt('12345'),
            'is_admin' => '1'
        ]);

        User::create([
            'name' => 'Mita',
            'username' => 'mitata',
            'password' => bcrypt('mita'),
            'is_admin' => '1'
        ]);
    }
}